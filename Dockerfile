# 1. Define our base image
FROM node:latest

# 2. Specify/create a working directory
WORKDIR /usr/src/app

# 3. Copy dependencies package.json and package-lock.json
COPY package*.json ./
RUN npm install

# 4. Bundle our sources - Copy it to the WORKDIR
COPY . .

# 5. Expose the PORT of our app to the outside
EXPOSE 8000

# 6. Run the command to start our app
CMD [ "node", "app.js" ]